﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {

    public Rigidbody2D rgdb;
    private Player scriptPlayer;
    private GameManager gManagerScript;
    private GameObject newBall;
    private Score scoreScript;
    private BlocksLives blScript;

    public Transform player;
    public GameObject[] powerUps;
    public GameObject explosion;
    public Image gameOverScreen;
    public Text finalScore;
    public GameObject ball;
    public AudioClip hitPaddle;
    public AudioClip hitOnFire;
    public AudioClip gameOver;
    public AudioClip loseBall;
    public AudioClip destroyBlock;
    public GameObject HighScores;

    public bool started = false;

    public float ballSpeed;
    private int valueSorted;

    // Use this for initialization
    void Start () {

        rgdb = GetComponent<Rigidbody2D>();

        scriptPlayer = GameObject.Find("Player").GetComponent<Player>();

        gManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        scoreScript = GameObject.Find("GreenBlock").GetComponent<Score>();

        blScript = GameObject.Find("GreenBlock").GetComponent<BlocksLives>();

    }

    // Update is called once per frame
    void Update ()
    {
        if (!started)
        {
            transform.position = new Vector2(player.position.x, transform.position.y);
        }

        if (Input.GetKeyDown(KeyCode.Space) && !started)
        {
            ballSpeed = 4;
            rgdb.velocity = new Vector2(Random.Range(-2f, 2f), ballSpeed);
            started = true;
        }

    }

    void FixedUpdate ()
    {
        //Avoid ball to keep itself with low y velocity (falling down slowly)
        if (started)
        {
            if (rgdb.velocity.y < 2 && rgdb.velocity.y > -2)
            {
                rgdb.gravityScale = 3;
            }
            else
            {
                rgdb.gravityScale = 0;
            }
        }
		
	}

    //Calculate the reflection of the Ball when it hits the platform
    float ballCollision (Vector2 ballPos, Vector2 playerPos, float playerWidth)
    {
        //  AudioSource.PlayClipAtPoint(hitOnFire, transform.position);
        return (ballPos.x - playerPos.x) / playerWidth;

    }

    //Check Collision with Game Objects with objects according to their tags
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "GreenBlocks" || collision.gameObject.tag == "BlueBlocks" || collision.gameObject.tag == "YellowBlocks" || collision.gameObject.tag == "OrangeBlocks" || collision.gameObject.tag == "RedBlocks")
        {
            ballSpeed = ballSpeed + 0.05f;

            if (scriptPlayer.isOnFire)
            {
                Instantiate(explosion, collision.transform.position, Quaternion.identity);
                AudioSource.PlayClipAtPoint(hitOnFire, transform.position);
            }
            else
            {
                AudioSource.PlayClipAtPoint(destroyBlock, transform.position);
            }

        } else if (collision.gameObject.tag == "Player" && started)
        {

            ballSpeed = ballSpeed + .05f;

            //Calling the function ballCollision with the paraments
            float calcResult = ballCollision(transform.position, collision.transform.position, ((BoxCollider2D)collision.collider).size.x);

            Vector2 newDir = new Vector2(calcResult, 1).normalized;

            rgdb.velocity = newDir * ballSpeed;
            if (started)
            {
                AudioSource.PlayClipAtPoint(hitPaddle, transform.position);
            }
            
        }

        if (collision.gameObject.tag == "DownBar")
        {
            AudioSource.PlayClipAtPoint(loseBall, transform.position);

            if (gManagerScript.lives > 1)
            {
                gManagerScript.lives -= 1;
                scriptPlayer.isOnFire = false;
                scriptPlayer.canShoot = false;
                transform.position = new Vector2(player.position.x, player.position.y + transform.lossyScale.y / 2);
                started = false;
                rgdb.velocity = Vector2.zero;
            }
            else
            {
                gManagerScript.lives -= 1;
                Destroy(gameObject);

                AudioSource.PlayClipAtPoint(gameOver, transform.position);
                gameOverScreen.enabled = true;
                PlayerPrefs.SetString("Player Name", "Foobar");
                string name = PlayerPrefs.GetString("Player Name");
                finalScore.enabled = true;

                finalScore.text = gManagerScript.score.ToString();
            }
            
        }
    }
}
