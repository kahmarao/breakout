﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    public GameManager gManagerScript;
    public Player scriptPlayer;
    public Ball ballScript;

    // Use this for initialization
    void Start()
    {
        gManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        scriptPlayer = GameObject.Find("Player").GetComponent<Player>();

        ballScript = GameObject.Find("Ball").GetComponent<Ball>();
    }

    private void OnDestroy()
    {


        gManagerScript.blocks--;

        if (gameObject.tag == "GreenBlocks")
        {
            gManagerScript.score = gManagerScript.score + 100;
        }

        if (gameObject.tag == "BlueBlocks")
        {
            gManagerScript.score = gManagerScript.score + 200;
        }

        if (gameObject.tag == "YellowBlocks")
        {
            gManagerScript.score = gManagerScript.score + 300;
        }

        if (gameObject.tag == "OrangeBlocks")
        {
            gManagerScript.score = gManagerScript.score + 500;
        }

        if (gameObject.tag == "RedBlocks")
        {
            gManagerScript.score = gManagerScript.score + 1000;
        }
    }
}
