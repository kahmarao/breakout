﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Rigidbody2D rgdb;
    private Animator pAnimation;
    private GameManager gManagerScript;
    private Ball ballScript;

    private float speed = 7;
    private float direction;
    private float timer = 0;

    public GameObject bullet;
    public GameObject bulletSpawn;
    public AudioClip getPowerUp;
    public AudioClip shootLaser;

    public bool canShoot;
    public bool isOnFire;

    // Use this for initialization
    void Start () {
        rgdb = GetComponent<Rigidbody2D>();
        pAnimation = GetComponent<Animator>();
        gManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        ballScript = GameObject.Find("Ball").GetComponent<Ball>();

        isOnFire = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Get the default Horizontal Inputs (Left and Right Arrow)
        direction = Input.GetAxisRaw("Horizontal");
        
        if (gManagerScript.lives > 0)
        {
            if (canShoot)
            {
                //Shoots using Left Ctrl by default
                if (Input.GetButtonDown("Fire1"))
                {
                    Instantiate(bullet, bulletSpawn.transform.position, Quaternion.identity);
                    AudioSource.PlayClipAtPoint(shootLaser, transform.position);
                }

                if (timer > 0) timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    canShoot = false;
                }
            }
        }
        
    }


    //Working with Physics
    private void FixedUpdate()
    {
        if (gManagerScript.lives > 0)
            rgdb.velocity = new Vector2(direction * speed, 0);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gManagerScript.lives > 0)
        {
            if (collision.tag == "puEnlarge")
            {
                pAnimation.Play("Enlarge");
                AudioSource.PlayClipAtPoint(getPowerUp, transform.position);
                gManagerScript.score = gManagerScript.score + 500;
                Destroy(collision.gameObject);
            }

            if (collision.tag == "puReduce")
            {
                pAnimation.Play("Reduce");
                AudioSource.PlayClipAtPoint(getPowerUp, transform.position);
                gManagerScript.score = gManagerScript.score + 1500;
                Destroy(collision.gameObject);
            }

            if (collision.tag == "puBullet")
            {
                if (!canShoot)
                {
                    timer = 7;
                    canShoot = true;
                }
                gManagerScript.score = gManagerScript.score + 100;
                AudioSource.PlayClipAtPoint(getPowerUp, transform.position);
                Destroy(collision.gameObject);
            }

            if (collision.tag == "puFire")
            {
                StartCoroutine(fireball(5));
                AudioSource.PlayClipAtPoint(getPowerUp, transform.position);
                gManagerScript.score = gManagerScript.score + 250;
                Destroy(collision.gameObject);
            }

            if (collision.tag == "puPlayer" && gManagerScript.lives<5)
            {
                gManagerScript.lives++;
                AudioSource.PlayClipAtPoint(getPowerUp, transform.position);
                gManagerScript.score = gManagerScript.score + 1500;
                Destroy(collision.gameObject);
            }

        }   
    }

    void ChangeAnim()
    {
        pAnimation.Play("Idle");
    }

    IEnumerator fireball(int waitingTime)
    {
        isOnFire = true;

        yield return new WaitForSeconds(waitingTime);

        isOnFire = false; 
    }
}
