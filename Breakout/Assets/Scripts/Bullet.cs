﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public AudioClip laserDestroy;

    private Rigidbody2D rgdb;

    private float initialSpeed = 10;

    // Use this for initialization
    void Start () {
		rgdb = GetComponent<Rigidbody2D>();

        rgdb.velocity = new Vector2(0, initialSpeed);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "GreenBlocks" || collision.gameObject.tag == "BlueBlocks" || collision.gameObject.tag == "YellowBlocks" || collision.gameObject.tag == "OrangeBlocks" || collision.gameObject.tag == "RedBlocks")
        {
            AudioSource.PlayClipAtPoint(laserDestroy, transform.position);
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Finish")
        {
            Destroy(gameObject);
        }


    }

}
