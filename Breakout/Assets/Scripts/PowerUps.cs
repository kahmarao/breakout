﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour {

    private Rigidbody2D rgdb;
    private float puSpeed = 3;

    // Use this for initialization
    void Start () {
        rgdb = GetComponent<Rigidbody2D>();

        rgdb.velocity = -Vector2.up * puSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "puDestroier")
        {
            Destroy(gameObject);
        }
    }
}
