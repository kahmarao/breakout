﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Destroy(gameObject, 0.5f);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "GreenBlocks" || collision.gameObject.tag == "BlueBlocks" || collision.gameObject.tag == "YellowBlocks" || collision.gameObject.tag == "OrangeBlocks" || collision.gameObject.tag == "RedBlocks")
        {
            Destroy(collision.gameObject);
        }
    }
}
