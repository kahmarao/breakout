﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject ball;
    public SpriteRenderer ballRender;
    public Sprite [] ballSprites;
    public Text scoreText;
    public Image livesSlider;

    public Ball ballScript;
    private Player scriptPlayer;
    private Scene scene;

    public int score;
    public int lives;
    public int blocks;


    // Use this for initialization
    void Awake () {

        DontDestroyOnLoad(this);
        
        scriptPlayer = GameObject.Find("Player").GetComponent<Player>();
        ballRender = GameObject.Find("Ball").GetComponent<SpriteRenderer>();
        ballScript = ball.GetComponent<Ball>();
        ballScript.started = false;
        ballScript.transform.position = new Vector2(scriptPlayer.transform.position.x, -4.6f);
        scriptPlayer.transform.position = new Vector2(0f, scriptPlayer.transform.position.y);

        scene = SceneManager.GetActiveScene();

        if (scene.name == "Level1")
        {
            score = 0;
        }

        lives = 3;

        livesSlider.fillAmount = (lives * 20) / 100.0f;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (score == 0)
            scoreText.text = "0000000" + score.ToString();
        else if (score < 1000)
            scoreText.text = "00000" + score.ToString();
        else if (score >= 1000 && score < 10000)
            scoreText.text = "0000" + score.ToString();
        else if (score >= 10000 && score < 100000)
            scoreText.text = "0000" + score.ToString();
        else if (score >= 100000 && score < 1000000)
            scoreText.text = "000" + score.ToString();
        else if (score >= 1000000 && score < 10000000)
            scoreText.text = "00" + score.ToString();
        else if (score >= 10000000 && score < 100000000)
            scoreText.text = "0" + score.ToString();
        else
            scoreText.text = score.ToString();

        livesSlider.fillAmount = (lives * 20) / 100.0f;

        if (lives > 0)
        {
            if (scriptPlayer.isOnFire)
            {
                ballRender.sprite = ballSprites[1];
            }
            else
            {
                ballRender.sprite = ballSprites[0];
            }
        }
        else
        {
            if (ballScript.gameOverScreen.enabled == true && Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                ShowHighScore();
            }
        }

        if(blocks < 1 )
        {
            if (SceneManager.GetActiveScene().buildIndex < 7)
            {
                ballScript.transform.position = new Vector2(ballScript.player.position.x, ballScript.player.position.y + transform.lossyScale.y / 2);
                ballScript.rgdb.velocity = Vector2.zero;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            } else
            {
                ballScript.gameOverScreen.enabled = true;
                ballScript.finalScore.enabled = true;
                //LoadHighScores(5);
                ballScript.finalScore.text = score.ToString();
            }
            
        }
            
	}

    private void OnLevelWasLoaded(int level)
    {
        
        foreach (Transform BlocksChildren in GameObject.Find("Blocks").transform)
        {
            blocks++;
        }

        scriptPlayer.isOnFire = false;
        scriptPlayer.canShoot = false;
    }

    private void ShowHighScore()
    {
        ballScript.gameOverScreen.enabled = false;
        ballScript.HighScores.SetActive(true);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            RestartGame();
        }
    }

    private void RestartGame()
    {
        ballScript.HighScores.SetActive(false);
        ballScript.finalScore.enabled = false;
        Destroy(GameObject.Find("GameManager"));
        SceneManager.LoadScene("Level1");
        
    }

    IEnumerator LoadHighScores(float time)
    {
        yield return new WaitForSeconds(time);

        ballScript.gameOverScreen.enabled = false;
        ballScript.HighScores.SetActive(true);
    }
}
