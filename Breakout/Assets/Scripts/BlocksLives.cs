﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocksLives : MonoBehaviour {

    public int lives;

    public AudioClip hitBlock;
    public Ball ballScript;

    private int valueSorted;
    private int chancePowerUp = 10;

    // Use this for initialization
    void Start () {
        ballScript = GameObject.Find("Ball").GetComponent<Ball>();

        if (gameObject.tag == "GreenBlocks")
        {
            lives = 1;
        }
        else if (gameObject.tag == "BlueBlocks")
        {
            lives = 2;
        }
        else if (gameObject.tag == "YellowBlocks")
        {
            lives = 3;
        }
        else if (gameObject.tag == "OrangeBlocks")
        {
            lives = 4;
        }
        else
        {
            lives = 5;
        }
    }
	
	// Update is called once per frame
	void Update () {
		if (lives <= 0)
        {
            Destroy(gameObject);

            valueSorted = Random.Range(0, 100);

            if (valueSorted <= chancePowerUp)
            {
                if (valueSorted == 0)
                {
                    Instantiate(ballScript.powerUps[4], gameObject.transform.position, Quaternion.identity);

                }
                else if (valueSorted == 1 || valueSorted == 2)
                {
                    Instantiate(ballScript.powerUps[3], gameObject.transform.position, Quaternion.identity);
                }
                else if (valueSorted == 3 || valueSorted == 4)
                {
                    Instantiate(ballScript.powerUps[2], gameObject.transform.position, Quaternion.identity);
                }
                else if (valueSorted == 5 || valueSorted == 6 || valueSorted == 7)
                {
                    Instantiate(ballScript.powerUps[1], gameObject.transform.position, Quaternion.identity);
                }
                else
                {
                    Instantiate(ballScript.powerUps[0], gameObject.transform.position, Quaternion.identity);
                }
            }
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Balls" || collision.gameObject.tag == "Explosion" || collision.gameObject.tag == "Bullet")
        {
            if (lives > 1)
            {
                AudioSource.PlayClipAtPoint(hitBlock, transform.position);
            }

            lives--;
        }
    }
}
